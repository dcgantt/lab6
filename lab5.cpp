#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  //initializes the deck of cards
  Card deck[52];
  //starts suit
  deck[0].value = 2;
  int i;
  //initializes the first 13 cards to be spades
  for(i = 1; i < 13; ++i) {
    deck[i].value = deck[i-1].value + 1;
    deck[i].suit = SPADES;
  }
  //begins next suit
  deck[13].value = 2;
  //initializes the second 13 cards to be hearts
  for(i = 14; i < 26; ++i) {
    deck[i].value = deck[i-1].value + 1;
    deck[i].suit = HEARTS;
  }
  //begins next suit
  deck[26].value = 2;
  //initializes the third 13 cards to be diamonds
  for(i = 27; i < 39; ++i) {
    deck[i].value = deck[i-1].value + 1;
    deck[i].suit = DIAMONDS;
  }
  //begins next suit
  deck[39].value = 2;
  //initializes the last 13 cards to be clubs
  for(i = 40; i < 52; ++i) {
    deck[i].value = deck[i-1].value + 1;
    deck[i].suit = CLUBS;
  }

  //we call random_shuffle() see the notes to determine the parameters to pass in.
   random_shuffle(deck, deck+ 52, myrandom);

   //builds a hand of 5 cards from the deck shuffled above
    Card hand[5];
    for (i = 0; i < 5; ++i) {
      hand[i].value = deck[i].value;
    }


    //sorts the cards
     sort(&hand[0], &hand[5], suit_order);

    //prints the hand
     for (i = 0; i < 5; ++i) {
       cout << setw(10) << right << get_card_name(hand[i]) << "of" << get_suit_code(hand[i]) << endl;
     }

  return 0;
}



//orders cards
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit == rhs.suit) {
    return lhs.value < rhs.value;
  }
  else {
    return lhs.suit < rhs.suit;
  }

}


//prints pattern of suits
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}


//Labels higher-ordered cards
string get_card_name(Card& c) {
  switch (c.value) {
  case 11:
    return "JACK";
  case 12:
    return "QUEEN";
  case 13:
    return "KING";
  case 14:
    return "ACE";
  default:
    return to_string(c.value);
  }
}
